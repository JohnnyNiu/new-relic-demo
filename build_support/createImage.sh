BASEDIR=$(dirname $(dirname "$0"))
cd $BASEDIR

mvn -DskipTests clean package

docker rmi -f nr_app1 2>/dev/null

docker build -t nr_app1 --rm=true .

docker run -d -p 8091:8091 --name nr_app1 nr_app1

# docker tag nr_app1:latest johnnyniu1/demo:nr_app1

# docker login --username=johnnyniu1 --email=johnny.niu@hotmail.com

# docker push johnnyniu1/demo:nr_app1