FROM java:8

RUN mkdir /usr/src/myapp
COPY ./target/newrelic-demo-0.0.1-SNAPSHOT.jar /usr/src/myapp

RUN mkdir /usr/newrelic
COPY ./newrelic /usr/newrelic

WORKDIR /usr/src/myapp
EXPOSE 8091:8091

CMD ["java", "-jar", "-javaagent:/usr/newrelic/newrelic.jar", "newrelic-demo-0.0.1-SNAPSHOT.jar" ]